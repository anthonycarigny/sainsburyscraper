**Compile the application**

The application can be compile with maven 3. 
Just run the command mvn clean package at the root of the project (where the pom.xml is)


**Run the application**

run the application by running the command : "java -jar scraper-0.0.1-SNAPSHOT.jar"



**to do next**

- format correctly the total as in the example. (ie "5.00" instead of "5.0")
- Complete the unit tests.
	Add mocks to unit test the app correctly.
	
