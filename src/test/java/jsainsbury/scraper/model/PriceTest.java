package jsainsbury.scraper.model;

import org.junit.Test;

import jsainsbury.scraper.model.Price;

import org.junit.Assert;

public class PriceTest {

	@Test
	public void should_return_total_gross_of_0() {
		Price total = new Price(0);
		Assert.assertEquals(0.00, total.getGross(), 0);
	}

	@Test
	public void should_return_total_gross_of_10() {
		Price total = new Price(10);
		Assert.assertEquals(10.00, total.getGross(), 0);
	}

	@Test
	public void should_return_VAT_of_0() {
		Price total = new Price(0);
		Assert.assertEquals(0.00, total.getVat(), 0);
	}

	@Test
	public void should_return_VAT_of_0_83() {
		Price total = new Price(5);
		Assert.assertEquals(0.83, total.getVat(), 0);
	}

}
