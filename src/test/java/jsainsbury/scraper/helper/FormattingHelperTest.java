package jsainsbury.scraper.helper;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jsainsbury.scraper.model.Product;
import jsainsbury.scraper.model.ProductSummary;

public class FormattingHelperTest {
	List<Product> products;
	ProductSummary productSummary;

	@Before
	public void setUp() throws Exception {
		products = new ArrayList<>();
		productSummary = new ProductSummary();
		Product strawberries = new Product();
		strawberries.setName("Sainsbury's Strawberries 400g");
		strawberries.setKcalPer100g(33);
		strawberries.setPrice(1.75);
		strawberries.setDescription("by Sainsbury's strawberries");

		products.add(strawberries);
		productSummary.setProducts(products);
	}

	@Test
	public void should_return_well_formated_json_without_total() throws Exception {
		String expected = "{\n" +
		/**/ "  \"results\": [\n" +
		/**/ "    {\n" +
		/**/ "      \"title\": \"Sainsbury's Strawberries 400g\",\n" +
		/**/ "      \"kcal_per_100g\": 33,\n" +
		/**/ "      \"unit_price\": 1.75,\n" +
		/**/ "      \"description\": \"by Sainsbury's strawberries\"\n" +
		/**/ "    }\n" +
		/**/ "  ]\n" +
		/**/ "}";
		String actual = FormattingHelper.createJSonResponse(productSummary);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void should_return_well_formated_json_with_total() throws Exception {
		String expected = "{\n" +
		/**/ "  \"results\": [\n" +
		/**/ "    {\n" +
		/**/ "      \"title\": \"Sainsbury's Strawberries 400g\",\n" +
		/**/ "      \"kcal_per_100g\": 33,\n" +
		/**/ "      \"unit_price\": 1.75,\n" +
		/**/ "      \"description\": \"by Sainsbury's strawberries\"\n" +
		/**/ "    }\n" +
		/**/ "  ]\n" +
		/**/ "}";
		String actual = FormattingHelper.createJSonResponse(productSummary);
		Assert.assertEquals(expected, actual);
	}

}
