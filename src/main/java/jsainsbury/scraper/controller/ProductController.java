package jsainsbury.scraper.controller;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import jsainsbury.scraper.helper.FormattingHelper;
import jsainsbury.scraper.helper.ScraperHelper;
import jsainsbury.scraper.model.Price;
import jsainsbury.scraper.model.Product;
import jsainsbury.scraper.model.ProductSummary;

public class ProductController {

	public void printSummary(String url, PrintStream printStream) throws IOException {
		FormattingHelper.createAndPrintJSonResponse(createSummary(url), printStream);
	}

	/**
	 * @param doc
	 * @return
	 * @throws IOException
	 */
	public ProductSummary createSummary(String url) throws IOException {
		Document doc;
		doc = ScraperHelper.getDocument(url);
		ProductSummary summary = new ProductSummary();

		Elements productsDom = doc.select("div.product");

		ArrayList<Product> products = new ArrayList<>();
		for (Element productDom : productsDom) {
			Product product = new Product();

			String title = productDom.selectFirst("h3").text();
			product.setName(title);

			String productLink = productDom.selectFirst("a").attr("abs:href");
			Document detailpage;
			try {
				detailpage = ScraperHelper.getDocument(productLink);

				Element nutritionTable = detailpage.getElementById("information").selectFirst(".nutritionTable");
				if (null != nutritionTable) {
					Element kcalPer100g = nutritionTable.select("tr").get(2).select("td").get(0);
					product.setKcalPer100g(Integer.valueOf(kcalPer100g.text().replace("kcal", "")));
				}

				Element productText = detailpage.getElementById("information").selectFirst(".productText");
				if (null != productText) {
					String description = productText.text().replace("Description ", "");
					product.setDescription(description);
				}
			} catch (IOException e) {
				System.err.print("there will be missing details for " + title + " : ");
				System.err.println("could not navigate to " + productLink);
			}

			Elements pricePerUnit = productDom.select(".pricePerUnit");
			String priceDom = pricePerUnit.first().ownText().replace("£", "");
			product.setPrice(Double.valueOf(priceDom));
			products.add(product);
		}

		summary.setProducts(products);
		summary.setTotal(new Price(products.stream().map(Product::getPrice).reduce((a, b) -> a + b).orElse(0.0)));
		return summary;
	}
}
