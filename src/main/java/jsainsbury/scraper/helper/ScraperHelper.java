package jsainsbury.scraper.helper;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class ScraperHelper {

	/**
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static Document getDocument(String url) throws IOException {
		return Jsoup.connect(url).get();
	}
}
