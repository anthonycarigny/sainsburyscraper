package jsainsbury.scraper.helper;

import java.io.PrintStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FormattingHelper {

	/**
	 * @param objectToSerialize
	 * @return
	 */
	public static String createJSonResponse(Object objectToSerialize) {
		final Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
		return gson.toJson(objectToSerialize);
	}

	/**
	 * @param objectToSerialize
	 * @return
	 */
	public static void createAndPrintJSonResponse(Object objectToSerialize, PrintStream printStream) {
		printStream.print(createJSonResponse(objectToSerialize));
	}
}
