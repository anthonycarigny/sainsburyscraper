package jsainsbury.scraper.model;

public class Price {
	private static final double VAT_PERCENTAGE = 0.2;
	private double gross;
	private double vat;

	public Price(double gross) {
		this.setGross(gross);
		this.setVat(gross - gross / (1 + VAT_PERCENTAGE));
	}

	public double getGross() {
		return gross;
	}

	public void setGross(double gross) {
		this.gross = Math.round(gross * 100d) / 100d;
	}

	public double getVat() {
		return vat;
	}

	private void setVat(double vat) {
		this.vat = Math.round(vat * 100d) / 100d;
	}

}
