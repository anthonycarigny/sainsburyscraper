package jsainsbury.scraper.model;

import com.google.gson.annotations.SerializedName;

public class Product {

	@SerializedName("title")
	private String name;
	@SerializedName("kcal_per_100g")
	private Integer kcalPer100g;
	@SerializedName("unit_price")
	private Double price;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getKcalPer100g() {
		return kcalPer100g;
	}

	public void setKcalPer100g(Integer kcalPer100g) {
		this.kcalPer100g = kcalPer100g;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
