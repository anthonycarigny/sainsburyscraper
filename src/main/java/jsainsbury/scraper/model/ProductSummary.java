package jsainsbury.scraper.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ProductSummary {
	@SerializedName("results")
	private List<Product> products;
	private Price total;

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Price getTotal() {
		return total;
	}

	public void setTotal(Price total) {
		this.total = total;
	}
}
