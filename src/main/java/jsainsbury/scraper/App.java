package jsainsbury.scraper;

import java.io.IOException;

import jsainsbury.scraper.controller.ProductController;

/**
 * Hello world!
 *
 */
public class App {
	static String SAINSBURYS_BERRIES_AND_CURRANTS_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";

	public static void main(String[] args) {
		ProductController productController = new ProductController();
		try {
			productController.printSummary(SAINSBURYS_BERRIES_AND_CURRANTS_URL, System.out);
		} catch (IOException e) {
			System.err.println("could not navigate to " + SAINSBURYS_BERRIES_AND_CURRANTS_URL);
		}
	}
}
